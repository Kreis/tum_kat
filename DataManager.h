#ifndef _DATA_MANAGER_H_
#define _DATA_MANAGER_H_

#include <iostream>
#include <vector>
#include <fstream>
#include <sys/stat.h>
#include <algorithm>
#include "Bint.h"
#include "base64.h"
#include "sha1.h"

class DataManager {
public:
	DataManager(std::string path_read);
	~DataManager();

	void set_writer(std::string path_write);
	void set_data(char* data, uint32_t size);
	void set_data(std::vector<char>& data);
	void set_data(std::vector<uint8_t>& data);

	std::vector<uint8_t>& get_data();
	std::vector<uint8_t>& get_data(uint64_t size);
	void set_in_buffer(char* buffer, uint64_t size);
	std::string get_line();
	std::vector<std::string> get_line(char separator);
	static std::vector<std::string> get_split(std::string text, char separator);
	uint64_t get_size();
	std::vector<uint8_t> get_name();
	std::string get_path();
	std::string get_dir();
	std::string get_format();
	std::vector<uint8_t> get_byte_hash();
	std::vector<uint8_t> get_byte_random_mask();
	std::vector<uint8_t> get_temporal_byte_hash();
	static uint64_t get_file_size(std::string path_file);
	static void write_single_data(std::string path_file, std::vector<std::string>& data);
	static std::string encrypt_plain_text(std::string message, Bint& e_key, Bint& public_key);
	static std::string decrypt_plain_text(std::string message, Bint& private_key, Bint& public_key);

	void rename_file(std::string name);
	void remove_temporal();
	void remove_readed();

	static uint8_t get_byte_value(char& A, char& B);

private:
	uint64_t data_size;

	std::vector<uint8_t> out_data;
	std::string path_in;
};

#endif