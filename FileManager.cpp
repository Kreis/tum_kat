#include "FileManager.h"



FileManager::FileManager() {
	pcripto = std::make_shared<Cripto>();
	name_unique_name = 0;
}

void FileManager::save_keys(std::vector<Bint> vec_keys, std::string name_keys, std::string password) {
	if (vec_keys.size() != 3)
		throw std::invalid_argument("Invalid number keys to save");

	Bint public_key 	= vec_keys[ 0 ];
	Bint e_key			= vec_keys[ 1 ];
	Bint private_key 	= vec_keys[ 2 ];

	std::vector<uint8_t>& vec_public_key 	= public_key.get_data();
	std::vector<uint8_t>& vec_e_key 		= e_key.get_data();
	std::vector<uint8_t>& vec_private_key 	= private_key.get_data();

	std::vector<uint8_t> complex_password = Cripto::get_complex_password(password);
	uint32_t id_complex_password = 0;
	for (uint32_t i = 0; i < vec_private_key.size(); i++) {
		vec_private_key[ i ] ^= complex_password[ id_complex_password ];
		id_complex_password = id_complex_password + 1 == complex_password.size() ? 0 : id_complex_password + 1;
	}
	
	std::string public_key_b64 		= base64_encode(&*vec_public_key.begin(), vec_public_key.size());
	std::string e_key_b64 			= base64_encode(&*vec_e_key.begin(), vec_e_key.size());
	std::string private_key_b64 	= base64_encode(&*vec_private_key.begin(), vec_private_key.size());

	std::vector<std::string> data;
	data.push_back("<P-KEY>");
	data.push_back(e_key_b64);
	data.push_back(public_key_b64);
	DataManager::write_single_data("public_" + name_keys, data);

	data.clear();
	data.push_back("<M-KEY>");
	data.push_back(private_key_b64);
	data.push_back(public_key_b64);
	DataManager::write_single_data("mask_" + name_keys, data);
}

uint64_t FileManager::count_file_r(std::string path_file_in, std::function<bool(std::string)> validation) {
	auto dir = opendir(path_file_in.c_str());
	uint64_t n_files = 0;
    if (dir == 0)
    	return 1;

    struct dirent* entity;
    while (true) {
    	entity = readdir(dir);
    	if (entity == 0)
    		break;

    	std::string entity_name = std::string(entity->d_name);
    	if (entity_name == "." || entity_name == ".." || entity_name == ".DS_Store")
    		continue;

		bool type_directory = entity->d_type == DT_DIR;
		bool type_registry  = entity->d_type == DT_REG;

		if (type_directory) {
			n_files += count_file_r(path_file_in + "/" + entity_name, validation);
			continue;
		}

		if (type_registry) {
			if ( ! validation(entity_name))
				continue;
			n_files++;
		}
    }

    closedir(dir);

    return n_files;
}

void FileManager::encrypt_file_r(
	std::string path_file_in,
	Bint& e_key,
	Bint& public_key,
	uint64_t total_files,
	uint64_t& current_files,
	std::function<bool(std::string)> validation,
	bool hard) {
	
	bytes_chunk = public_key.get_data().size() - 1;

    auto dir = opendir(path_file_in.c_str());

    if (dir == 0) {
    	std::shared_ptr<DataManager> pdata_manager = std::make_shared<DataManager>(path_file_in);
		encrypt_file(pdata_manager, e_key, public_key, hard);
    	return;
    }

    struct dirent* entity;
    while (true) {
    	entity = readdir(dir);
    	if (entity == 0)
    		break;

    	std::string entity_name = std::string(entity->d_name);
    	if (entity_name == "." || entity_name == ".." || entity_name == ".DS_Store")
    		continue;

		bool type_directory = entity->d_type == DT_DIR;
		bool type_registry  = entity->d_type == DT_REG;

		if (type_directory) {
			encrypt_file_r(path_file_in + "/" + entity_name, e_key, public_key, total_files, current_files, validation, hard);
			continue;
		}

		if (type_registry) {
			bool is_already_encrypted = ! validation(entity_name);
			if (is_already_encrypted)
				continue;
			
			std::shared_ptr<DataManager> pdata_manager = std::make_shared<DataManager>(path_file_in + "/" + entity_name);
			encrypt_file(pdata_manager, e_key, public_key, hard);

			current_files++;
			std::cout << current_files << "/" << total_files << std::endl;
			continue;
		}
    }

    closedir(dir);
}

void FileManager::decrypt_file_r(
	std::string path_file_in,
	Bint& private_key,
	Bint& public_key,
	uint64_t total_files,
	uint64_t& current_files,
	std::function<bool(std::string)> validation) {

	bytes_chunk = public_key.get_data().size() - 1;

    auto dir = opendir(path_file_in.c_str());

    if (dir == 0) {
    	std::shared_ptr<DataManager> pdata_manager = std::make_shared<DataManager>(path_file_in);
		decrypt_file(pdata_manager, private_key, public_key);
    	return;
    }

    struct dirent* entity;
    while (true) {
    	entity = readdir(dir);
    	if (entity == 0)
    		break;

    	std::string entity_name = std::string(entity->d_name);
    	if (entity_name == "." || entity_name == ".." || entity_name == ".DS_Store")
    		continue;

		bool type_directory = entity->d_type == DT_DIR;
		bool type_registry  = entity->d_type == DT_REG;

		if (type_directory) {
			decrypt_file_r(path_file_in + "/" + entity_name, private_key, public_key, total_files, current_files, validation);
			continue;
		}

		if (type_registry) {
			if ( ! validation(entity_name))
				continue;

			std::shared_ptr<DataManager> pdata_manager = std::make_shared<DataManager>(path_file_in + "/" + entity_name);
			decrypt_file(pdata_manager, private_key, public_key);

			current_files++;
			std::cout << current_files << "/" << total_files << std::endl;
			continue;
		}
    }
}

void FileManager::encrypt_file(
	std::shared_ptr<DataManager>& pdata_manager,
	Bint& e_key,
	Bint& public_key,
	bool hard) {
	
	std::string path_dir = pdata_manager->get_dir();
	std::string new_name = get_unique_name(path_dir, hard);
	new_name += hard ? ".kath" : ".kat";
	pdata_manager->set_writer(path_dir + new_name);

	std::vector<uint8_t> name_byte 	= pdata_manager->get_name();
	std::vector<uint8_t> hash_tye 	= pdata_manager->get_byte_hash();
	std::vector<uint8_t> all_metadata;
	uint8_t total_chunks = 0;
	int16_t total_bytes = name_byte.size() + hash_tye.size() + 1 + 2;
	while (total_bytes > 0) {
		total_bytes -= (bytes_chunk - 1);
		total_chunks++;
	}
	all_metadata.push_back(total_chunks);
	all_metadata.push_back(name_byte.size());
	all_metadata.insert(all_metadata.end(), name_byte.begin(), name_byte.end());
	all_metadata.push_back(hash_tye.size());
	all_metadata.insert(all_metadata.end(), hash_tye.begin(), hash_tye.end());

	encrypt_file_metadata(all_metadata, pdata_manager, e_key, public_key);

	if (hard) {
		encrypt_file_data_hard(pdata_manager, e_key, public_key);
	} else {
		std::vector<uint8_t> random_mask = pdata_manager->get_byte_random_mask();
		all_metadata.clear();
		total_chunks = 0;
		total_bytes = random_mask.size() + 1 + 1;
		while (total_bytes > 0) {
			total_bytes -= (bytes_chunk - 1);
			total_chunks++;
		}
		all_metadata.push_back(total_chunks);
		all_metadata.push_back(random_mask.size());
		all_metadata.insert(all_metadata.end(), random_mask.begin(), random_mask.end());
		encrypt_file_metadata(all_metadata, pdata_manager, e_key, public_key);

		encrypt_file_data_soft(pdata_manager, random_mask);
	}
	
	pdata_manager->remove_readed();
}

void FileManager::decrypt_file(
	std::shared_ptr<DataManager>& pdata_manager,
	Bint& private_key,
	Bint& public_key) {
	
	std::vector<std::vector<uint8_t> > metadata = decrypt_file_metadata(pdata_manager, private_key, public_key);

	std::string format = pdata_manager->get_format();
	if (format == "kat") {
		std::vector<uint8_t> random_mask = decrypt_file_metadata(pdata_manager, private_key, public_key)[ 0 ];
		decrypt_file_data_soft(metadata, pdata_manager, random_mask);
	} else if (format == "kath") {
		decrypt_file_data_hard(metadata, pdata_manager, private_key, public_key);
	} else {
		throw std::runtime_error("Invalid format to decrypt");
	}

	pdata_manager->remove_readed();
}

std::pair<Bint, Bint> FileManager::get_public_keys_by_file(std::string path_file) {
	
	std::shared_ptr<DataManager> pdata_manager = std::make_shared<DataManager>(path_file);
	std::vector<std::string> info_public_key = pdata_manager->get_line('*');


	if (info_public_key[ 0 ] != "<P-KEY>")
		throw std::runtime_error("Invalid public key");

	std::string string_e_key_b64 		= info_public_key[ 1 ];
	std::string string_public_key_b64 	= info_public_key[ 2 ];

	std::vector<uint8_t> vec_e_key 		= base64_decode(string_e_key_b64);
	std::vector<uint8_t> vec_public_key = base64_decode(string_public_key_b64);

	return std::make_pair(Bint(vec_e_key), Bint(vec_public_key));
}

std::pair<Bint, Bint> FileManager::get_private_keys_by_file(std::string path_file, std::string password) {

	std::shared_ptr<DataManager> pdata_manager = std::make_shared<DataManager>(path_file);
	std::vector<std::string> info_private_key = pdata_manager->get_line('*');

	if (info_private_key[ 0 ] != "<M-KEY>")
		throw std::runtime_error("Invalid mask key");

	std::string string_private_key_b64 		= info_private_key[ 1 ];
	std::string string_public_key_b64		= info_private_key[ 2 ];

	std::vector<uint8_t> vec_private_key	= base64_decode(string_private_key_b64);
	std::vector<uint8_t> vec_public_key		= base64_decode(string_public_key_b64);

	std::vector<uint8_t> complex_password = Cripto::get_complex_password(password);
	uint8_t id_complex_password = 0;
	for (uint32_t i = 0; i < vec_private_key.size(); i++) {
 		vec_private_key[ i ] ^= complex_password[ id_complex_password ];
 		id_complex_password = id_complex_password + 1 == complex_password.size() ? 0 : id_complex_password + 1;
	}

	return std::make_pair(Bint(vec_private_key), Bint(vec_public_key));
}

void FileManager::encrypt_file_metadata(
	std::vector<uint8_t>& metadata,
	std::shared_ptr<DataManager>& pdata_manager,
	Bint& e_key,
	Bint& public_key) {

	uint32_t metadata_size = metadata.size();

	uint32_t id_metadata = 0;
	while (metadata_size > 0) {
		uint32_t current_chunk = metadata_size >= bytes_chunk - 1 ? bytes_chunk - 1 : metadata_size;

		std::vector<uint8_t> metadata_chunk(metadata.begin() + id_metadata, metadata.begin() + id_metadata + current_chunk);
		metadata_chunk.push_back(1);
		Bint data(metadata_chunk);

		Bint encrypt_data = Cripto::encrypt(data, e_key, public_key);
		std::vector<uint8_t>& vector_encrypt_data = encrypt_data.get_data();

		while (vector_encrypt_data.size() < bytes_chunk + 1)
			vector_encrypt_data.push_back( 0 );

		pdata_manager->set_data(vector_encrypt_data);

		metadata_size -= current_chunk;
		id_metadata += current_chunk;
	}
}

void FileManager::encrypt_file_data_hard(
	std::shared_ptr<DataManager>& pdata_manager,
	Bint& e_key,
	Bint& public_key) {

	uint64_t readed_pointer = 0;
	uint64_t file_size 		= pdata_manager->get_size();

	while (file_size) {

		uint8_t current_chunk = file_size > bytes_chunk - 1 ? bytes_chunk - 1 : file_size;
		std::vector<uint8_t>& vector_data = pdata_manager->get_data(current_chunk);
		vector_data.push_back(1);
		Bint data(vector_data);
		std::vector<uint8_t> vector_encrypt_data = Cripto::encrypt(data, e_key, public_key).get_data();
		while (vector_encrypt_data.size() < bytes_chunk + 1)
			vector_encrypt_data.push_back(0);
		
		pdata_manager->set_data(vector_encrypt_data);

		file_size -= current_chunk;
	}
}

std::vector<std::vector<uint8_t> > FileManager::decrypt_file_metadata(
	std::shared_ptr<DataManager>& pdata_manager,
	Bint& private_key,
	Bint& public_key) {
	
	std::vector<std::vector<uint8_t> > vector_metadatas;
	std::vector<uint8_t> vector_decrypt_metadata;
	uint32_t total_chunks = 0;
	uint32_t id_data = 1;
	do {
		std::vector<uint8_t>& vector_data = pdata_manager->get_data(bytes_chunk + 1);
		Bint data(vector_data);
		std::vector<uint8_t> vector_decrypt_data = pcripto->decrypt(data, private_key, public_key).get_data();
		uint8_t decrypt_data_size = vector_decrypt_data.size() - id_data - 1;

		total_chunks = total_chunks == 0 ? vector_decrypt_data[ 0 ] : total_chunks;

		vector_decrypt_metadata.insert(vector_decrypt_metadata.end(),
			vector_decrypt_data.begin() + id_data,
			vector_decrypt_data.begin() + id_data + decrypt_data_size);

		id_data = 0;
		total_chunks--;
	} while (total_chunks);

	id_data = 0;
	while (vector_decrypt_metadata[ id_data ]) {
		uint8_t chunk_size = vector_decrypt_metadata[ id_data ];
		id_data++;

		std::vector<uint8_t> current_chunk(
		vector_decrypt_metadata.begin() + id_data,
		vector_decrypt_metadata.begin() + id_data + chunk_size);

		vector_metadatas.push_back(current_chunk);

		id_data += chunk_size;
	}

	return vector_metadatas;
}

void FileManager::decrypt_file_data_hard(
	std::vector<std::vector<uint8_t> >& metadata,
	std::shared_ptr<DataManager>& pdata_manager,
	Bint& private_key,
	Bint& public_key) {

	pdata_manager->set_writer(pdata_manager->get_dir() + "tum_kat_temp");
	uint64_t file_size = pdata_manager->get_size();

	while (file_size > 0) {

		std::vector<uint8_t>& vector_data = pdata_manager->get_data(bytes_chunk + 1);
		Bint data(vector_data);

		Bint decrypt_data = pcripto->decrypt(data, private_key, public_key);
		decrypt_data.print();
		std::vector<uint8_t> vector_data_decrypt = decrypt_data.get_data();
		vector_data_decrypt.resize(vector_data_decrypt.size() - 1);
		
		pdata_manager->set_data(vector_data_decrypt);

		file_size -= (bytes_chunk + 1);
	}

	std::vector<uint8_t> new_file_hash = pdata_manager->get_temporal_byte_hash();

	if (new_file_hash != metadata[ 1 ]) {
		pdata_manager->remove_temporal();
		throw std::runtime_error("invalid decrypted file");
	}

	std::string original_name = "";
	for (uint8_t& v : metadata[ 0 ])
		original_name += v;

	pdata_manager->rename_file(original_name);
}

void FileManager::encrypt_file_data_soft(
	std::shared_ptr<DataManager>& pdata_manager,
	std::vector<uint8_t>& random_mask) {

	uint32_t mask_size = random_mask.size();
	uint32_t id_mask = 0;
	uint8_t last = 0;
	uint8_t last2 = 0;
	uint64_t file_size = pdata_manager->get_size();
	while (file_size > 0) {
		uint32_t current_chunk = file_size > 100000 ? 100000 : file_size;
		char buffer[100000];
		pdata_manager->set_in_buffer(buffer, current_chunk);

		for (uint32_t i = 0; i < current_chunk; i++) {
			last = buffer[ i ];
			buffer[ i ] += last2;
			last2 = last;
			buffer[ i ] ^= random_mask[ id_mask ];
			id_mask = id_mask + 1 >= mask_size ? 0 : id_mask + 1;
		}

		pdata_manager->set_data(buffer, current_chunk);
		file_size -= current_chunk;
	}
}

void FileManager::decrypt_file_data_soft(
		std::vector<std::vector<uint8_t> >& metadata,
		std::shared_ptr<DataManager>& pdata_manager,
		std::vector<uint8_t>& random_mask) {

	uint32_t mask_size = random_mask.size();
	uint32_t id_mask = 0;
	uint8_t last = 0;
	pdata_manager->set_writer(pdata_manager->get_dir() + "tum_kat_temp");

	uint64_t file_size = pdata_manager->get_size();
	while (file_size > 0) {

		uint32_t current_chunk = file_size > 100000 ? 100000 : file_size;

		char buffer[100000];
		pdata_manager->set_in_buffer(buffer, current_chunk);

		for (uint32_t i = 0; i < current_chunk; i++) {
			buffer[ i ] ^= random_mask[ id_mask ];
			buffer[ i ] -= last;
			last = buffer[ i ];
			id_mask = id_mask + 1  >= mask_size ? 0 : id_mask + 1;
		}

		pdata_manager->set_data(buffer, current_chunk);

		file_size -= current_chunk;
	}

	std::vector<uint8_t> new_file_hash = pdata_manager->get_temporal_byte_hash();

	if (new_file_hash != metadata[ 1 ]) {
		pdata_manager->remove_temporal();
		throw std::runtime_error("invalid decrypted file");
	}

	std::string original_name = "";
	for (uint8_t& v : metadata[ 0 ])
		original_name += v;

	pdata_manager->rename_file(original_name);
}

std::string FileManager::get_unique_name(std::string path_dir, bool hard) {

	std::string string_name;
	bool is_good = true;

	while (is_good) {

		string_name = std::to_string(name_unique_name);
		std::ifstream infile((path_dir + string_name + (hard ? ".kath" : ".kat")).c_str());
		is_good = infile.good();
		
		name_unique_name++;
	}

	return string_name;
}





