#include "Cripto.h"

Cripto::Cripto() {
	srand(time(NULL));
	ZERO = Bint(0);
	ONE = Bint(1);
	TWO = Bint(2);
}

std::vector<Bint> Cripto::generate_keys(uint64_t n_bits_of_key) {

	std::pair<Bint, Bint> p_q = generate_p_q(n_bits_of_key);
	Bint p = p_q.first;
	Bint q = p_q.second;

	Bint public_key = p * q;
	
	p.mm();
	q.mm();

	Bint pq_1 = p * q;
	Bint e = generate_e(pq_1);
	Bint private_key = generate_private_key(pq_1, e);

	if ( ! final_test(e, private_key, public_key))
		return generate_keys(n_bits_of_key);

	std::vector<Bint> vec;
	vec.push_back(public_key);
	vec.push_back(e);
	vec.push_back(private_key);

	return vec;
}

Bint Cripto::generate_e(Bint& pq_1) {
	uint64_t e = get_random(10);

	while ( ! validate_e(pq_1, e))
		e += 2;

	return Bint( e );
}

bool Cripto::validate_e(Bint& pq_1, uint64_t e) {
	
	if ( ! is_prime( e ))
		return false;

	Bint bint_e( e );
	if (pq_1 % bint_e == ZERO)
		return false;

	return true;
}

void Cripto::generate_sieve_primes(uint32_t limit) {
	std::vector<bool> v(limit);
	for (uint32_t i = 2; i*i < limit; i++)
		if ( ! v[ i ])
			for (int j = i*2; j < limit; j+=i)
				v[ j ] = true;

	for (uint32_t i = 2; i < limit; i++) {
		if ( ! v[ i ])
			primes.push_back(Bint( i ));
	}
}

std::pair<Bint, Bint> Cripto::generate_p_q(uint64_t n_bits_of_key) {
	n_bits_of_key /= 2;
	n_bits_of_key /= 8;

	generate_sieve_primes(1224);
	return std::make_pair( generate_prime_number(n_bits_of_key), generate_prime_number(n_bits_of_key) );
}

bool Cripto::final_test(Bint& e_key, Bint& private_key, Bint& public_key) {
	uint64_t r = get_random(30);
	Bint data(r);

	Bint encrypted = encrypt(data, e_key, public_key);
	Bint decrypted = decrypt(encrypted, private_key, public_key);

	return data == decrypted;
}

bool Cripto::miller_rabin_individual_test(Bint n, Bint d, uint64_t int_k) {
	std::vector<uint8_t> vec_random = n.get_data();
	uint8_t len = vec_random.size() - 1;
	vec_random[ len ] = rand() % vec_random[ len ];
	for (int32_t i = len - 1; i >= 0; i--)
		vec_random[ i ] = rand() % 256;
	vec_random[ 0 ] |= 1;

	Bint a(vec_random);
	Bint b = a.spow(d, n);
	if (b == ONE)
		return true;

	Bint pp_minus1 = n;
	pp_minus1.mm();
	if (b == pp_minus1)
		return true;

	for (uint64_t i = 1; i < int_k; i++) {
		b = b.spow(TWO, n);

		if (b == pp_minus1)
			return true;

		if (b == ONE)
			return false;
	}
	
	return false;
}

bool Cripto::miller_rabin(Bint n, uint64_t i) {

	Bint n_mm = n;
	n_mm.mm();

	uint64_t int_k = 1;

	while (true) {
		if ( ! n_mm.is_pair())
			break;

		n_mm.bit_right();
		int_k++;
	}
	

	while (i-- >= 1) {
		if ( ! miller_rabin_individual_test(n, n_mm, int_k))
			return false;
	}

	return true;
}

bool Cripto::fermat_little(Bint n, uint64_t i) {
	while (i-- > 1) {
		Bint a((rand() % 10000) + 3);

		Bint b = a.spow(n, n);

		if (b != a)
			return false;
	}

	return true;
}

Bint Cripto::generate_prime_number(uint64_t n_bytes) {
	std::vector<uint8_t> vec_random;
	for (uint64_t i = 0; i < n_bytes; i++)
		vec_random.push_back(rand() % 256);
	vec_random[ 0 ] |= 3;

	Bint n(vec_random);
	Bint safe_n;
	
	while (true) {
		n.pp();
		n.pp();
		safe_n = n;
		safe_n.mm();
		safe_n.bit_right();

		bool first_test_success = true;
		for (uint32_t i = 0; i < 200; i++) {
			Bint mod = n % primes[ i ];
			Bint safe_mod = safe_n % primes[ i ];
			if (mod == ZERO || safe_mod == ZERO) {
				first_test_success = false;
				break;
			}
		}
		if ( ! first_test_success) {
			continue;
		}

		if ( ! fermat_little(n, 4) || ! fermat_little(safe_n, 4)) {
			std::cout << "-";
			continue;
		}

		if (miller_rabin(n, 16) && miller_rabin(safe_n, 16)) {
			std::cout << "+" << std::endl;
			return n;
		}

		std::cout << "-" << std::endl;
	}

}

Bint Cripto::generate_private_key(Bint& pq_1, Bint& e) {

	Bint d = ZERO;
	while (true) {
		d += pq_1;
		Bint d_1 = d + ONE;

		std::pair<Bint,Bint> div;
		d_1.div_operation(e, div);

		if (div.second == ZERO)
			return div.first;
	}
	
	return ZERO;
}

Bint Cripto::encrypt(Bint& data, Bint& e_key, Bint& public_key) {
	return data.spow(e_key, public_key);
}

Bint Cripto::decrypt(Bint& data, Bint& private_key, Bint& public_key) {
	return data.spow(private_key, public_key);
}

std::vector<uint8_t> Cripto::get_complex_password(std::string password) {
	
	std::vector<uint8_t> complex_password;
	std::string hash_password = sha1(password);
	for (int32_t i = hash_password.length() - 1; i > 0; i -= 2) {
		uint8_t current_byte = DataManager::get_byte_value(hash_password[ i - 1], hash_password[ i ]);
		complex_password.push_back(current_byte);
	}

	return complex_password;
}

bool Cripto::is_prime(uint64_t num) {
	if (num == 2 || num == 3)
		return true;
	if (num % 2 == 0 || num % 3 == 0)
		return false;

	uint64_t i = 5;
	uint64_t w = 2;

	while (i*i <= num) {
		if (num % i == 0)
			return false;

		i += w;
		w = 6 - w;
	}

	return true;
}

// private
Bint Cripto::gcd(Bint& A, Bint& B) {

	if (B == ZERO)
		return A;

	Bint mod = A % B;

	return gcd(B, mod);
}

uint64_t Cripto::get_random(uint32_t num_bits) {

	uint64_t r = 0;
	
	for (uint8_t i = 0; i < num_bits; i++)
		r = (r << 1) | (rand() % 2);

	if (num_bits > 8)
		r |= (1 << (num_bits - 8));

	r |= 1;
	
	return r;
}