#include <iostream>
#include "sha1.h"
#include "Cripto.h"
#include "FileManager.h"
#include "DataManager.h"
#include "base64.h"
#include <math.h>

#ifdef __WIN32
#include <windows.h>
void echo_off() {
	HANDLE hStdin = GetStdHandle(STD_INPUT_HANDLE); 
	DWORD mode = 0;
	GetConsoleMode(hStdin, &mode);
	SetConsoleMode(hStdin, mode & (~ENABLE_ECHO_INPUT));
}
#else
#include <termios.h>
#include <unistd.h>
void echo_off() {
	termios oldt;
    tcgetattr(STDIN_FILENO, &oldt);
    termios newt = oldt;
    newt.c_lflag &= ~ECHO;
    tcsetattr(STDIN_FILENO, TCSANOW, &newt);
}
#endif

std::string get_password(bool pv) {
	if ( ! pv)
		echo_off();

	std::string pass;
	std::cout << "Enter password" << std::endl;
	std::cin >> pass;

	return pass;
}

void tum_kat_generate_keys(std::string name_keys, uint64_t n_bits_of_key, bool pv) {
	Cripto cripto;

	std::vector<Bint> vec_keys = cripto.generate_keys(n_bits_of_key);
	FileManager::save_keys(vec_keys, name_keys, get_password(pv));
	std::cout << "generated keys" << std::endl;
}

void tum_kat_simple_enc() {
	std::cout << "Insert Public-Key:" << std::endl;
	std::string info_public_key_string;
	std::getline(std::cin, info_public_key_string);
	std::vector<std::string> info_public_key = DataManager::get_split(info_public_key_string, '*');
	
	std::string title_public = info_public_key[ 0 ];
	if (title_public != "<P-KEY>") {
		std::cout << "Invalid public key" << std::endl;
		return;
	}

	std::string e_key_string = info_public_key[ 1 ];
	std::string public_key_string = info_public_key[ 2 ];

	std::cout << "Insert message:" << std::endl;
	
	std::string message_string;
	std::getline(std::cin, message_string);

	std::vector<uint8_t> vector_e_key = base64_decode(e_key_string);
	Bint e_key(vector_e_key);
	
	std::vector<uint8_t> vector_public_key = base64_decode(public_key_string);
	Bint public_key(vector_public_key);

	std::string encrypt_message_base64 = DataManager::encrypt_plain_text(message_string, e_key, public_key);

	std::cout << "Encrypted message:" << std::endl;
	std::cout << encrypt_message_base64 << std::endl;
}

void tum_kat_simple_dec(bool pv) {
	std::cout << "Insert Mask-Key:" << std::endl;
	std::string info_mask_key_string;
	std::getline(std::cin, info_mask_key_string);

	std::vector<std::string> info_mask_key = DataManager::get_split(info_mask_key_string, '*');
	std::string title_mask = info_mask_key[ 0 ];

	if (title_mask != "<M-KEY>") {
		std::cout << "Invalid mask key" << std::endl;
	}
	std::string mask_key_string = info_mask_key[ 1 ];
	std::string public_key_string = info_mask_key[ 2 ];

	std::cout << "Insert encoded message:" << std::endl;
	std::string message_string;
	std::getline(std::cin, message_string);

	std::string password = get_password(pv);

	std::vector<uint8_t> vector_mask_key = base64_decode(mask_key_string);
	std::vector<uint8_t> complex_password = Cripto::get_complex_password(password);
	uint32_t id_complex_password = 0;
	for (uint32_t i = 0; i < vector_mask_key.size(); i++) {
 		vector_mask_key[ i ] ^= complex_password[ id_complex_password ];
 		id_complex_password = id_complex_password + 1 == complex_password.size() ? 0 : id_complex_password + 1;
	}
	Bint private_key(vector_mask_key);

	std::vector<uint8_t> vector_public_key = base64_decode(public_key_string);
	Bint public_key(vector_public_key);

	std::string decrypt_message_string = DataManager::decrypt_plain_text(message_string, private_key, public_key);

	std::cout << "Decrypted message:" << std::endl;
	std::cout << decrypt_message_string << std::endl;
}

void tum_kat_encode(std::string path_destiny, std::string path_public_key, bool hard = false) {
	FileManager file_manager;
	std::pair<Bint,Bint> public_keys = file_manager.get_public_keys_by_file(path_public_key);
	Bint& e_key 		= public_keys.first;
	Bint& public_key 	= public_keys.second;

	std::function<bool(std::string)> validation = [](std::string name_file) {
		if (name_file.length() < 5)
			return true;
		if (name_file.substr(name_file.length() - 5) != ".kath" && name_file.substr(name_file.length() - 4) != ".kat")
			return true;
		return false;
	};

	uint64_t total_files = file_manager.count_file_r(path_destiny, validation);
	uint64_t current_files = 0;
	file_manager.encrypt_file_r(path_destiny, e_key, public_key, total_files, current_files, validation, hard);
}

void tum_kat_decode(std::string path_destiny, std::string path_mask_key, bool pv) {
	FileManager file_manager;
	std::pair<Bint, Bint> private_keys = file_manager.get_private_keys_by_file(path_mask_key, get_password(pv));
	Bint& private_key 	= private_keys.first;
	Bint& public_key 	= private_keys.second;

	std::function<bool(std::string)> validation = [](std::string name_file) {
		if (name_file.length() < 5)
			return false;
		if (name_file.substr(name_file.length() - 5) != ".kath" && name_file.substr(name_file.length() - 4) != ".kat")
			return false;
		return true;
	};

	uint64_t total_files = file_manager.count_file_r(path_destiny, validation);
	uint64_t current_files = 0;
	file_manager.decrypt_file_r(path_destiny, private_key, public_key, total_files, current_files, validation);
}

// menu
int main(int argc, char** argv) {
std::cout << "start" << std::endl;
	
	std::clock_t begin_time = std::clock();
	if (argc == 2 && std::string(argv[ 1 ]) == "-h") {
		std::cout << "Instructions generate key:" << std::endl;
		std::cout << "[ 0 ] Command (generate)" << std::endl;
		std::cout << "[ 1 ] Name keys" << std::endl;
		std::cout << "[ 2 ] Number bits of the key" << std::endl;
		std::cout << "[ 3 ] pv (optional) password visible while typing" << std::endl;
		std::cout << std::endl;
		std::cout << "Instructions encode:" << std::endl;
		std::cout << "[ 0 ] Command (enc)" << std::endl;
		std::cout << "[ 1 ] Path destiny" << std::endl;
		std::cout << "[ 2 ] Path public key" << std::endl;
		std::cout << "[ 3 ] hard (optional) use only RSA algorithm" << std::endl;
		std::cout << std::endl;
		std::cout << "Instructions decode:" << std::endl;
		std::cout << "[ 0 ] Command (dec)" << std::endl;
		std::cout << "[ 1 ] Path destiny" << std::endl;
		std::cout << "[ 2 ] Path mask key" << std::endl;
		std::cout << "[ 3 ] pv (optional) password visible while typing" << std::endl;
		std::cout << std::endl;
		std::cout << "Instructions simple encode:" << std::endl;
		std::cout << "[ 0 ] Command (simple)" << std::endl;
		std::cout << "[ 1 ] Command (enc)" << std::endl;
		std::cout << "follow the instructions, required string tum_kat keys generated" << std::endl;
		std::cout << std::endl;
		std::cout << "Instructions simple decode:" << std::endl;
		std::cout << "[ 0 ] Command (simple)" << std::endl;
		std::cout << "[ 1 ] Command (dec)" << std::endl;
		std::cout << "[ 2 ] pv (optional) password visible while typing" << std::endl;
		std::cout << "follow the instructions, required string tum_kat keys generated" << std::endl;
		std::cout << std::endl;
		return 0;
	}

	if (argc >= 4 && std::string(argv[ 1 ]) == "generate") {
		std::string name_keys 				= argv[ 2 ];
		std::string string_n_bits_of_key 	= argv[ 3 ];
		uint64_t n_bits_of_key = atoi(string_n_bits_of_key.c_str());
		bool pv 							= argc >= 5 && std::string(argv[ 4 ]) == "pv";
		tum_kat_generate_keys(name_keys, n_bits_of_key, pv);
		std::cout << float( std::clock () - begin_time ) /  CLOCKS_PER_SEC;
		return 0;
	}

	if (argc >= 3 && std::string(argv[ 1 ]) == "simple" && std::string(argv[ 2 ]) == "enc") {
		tum_kat_simple_enc();
		return 0;
	}

	if (argc >= 3 && std::string(argv[ 1 ]) == "simple" && std::string(argv[ 2 ]) == "dec") {
		bool pv = argc >= 4 && std::string(argv[ 3 ]) == "pv";
		tum_kat_simple_dec(pv);
		return 0;
	}

	if (argc < 4) {
		std::cout << "Invalid execution\nuse -h" << std::endl;
		std::cout << float( std::clock () - begin_time ) /  CLOCKS_PER_SEC;
		return 0;
	}
	
	std::string command 		= argv[ 1 ];
	std::string path_destiny 	= argv[ 2 ];
	std::string path_tool		= argv[ 3 ];
	bool pv						= argc >= 5 && std::string(argv[ 4 ]) == "pv";
	bool hard					= argc >= 5 && std::string(argv[ 4 ]) == "hard";

	if (command == "enc") {
		tum_kat_encode(path_destiny, path_tool, hard);
		std::cout << float( std::clock () - begin_time ) /  CLOCKS_PER_SEC;
		return 0;
	}

	if (command == "dec") {
		tum_kat_decode(path_destiny, path_tool, pv);
		std::cout << float( std::clock () - begin_time ) /  CLOCKS_PER_SEC;
		return 0;
	}

	std::cout << "Invalid execution\nuse -h" << std::endl;
	std::cout << float( std::clock () - begin_time ) /  CLOCKS_PER_SEC;
	return 0;	
}