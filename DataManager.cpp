#include "DataManager.h"
#include "Cripto.h"

struct stat results;
static std::ifstream read_file;
static std::ofstream write_file;
DataManager::DataManager(std::string path_read) {
	srand(time(NULL));
	path_in = path_read;
	read_file.open(path_read);
	data_size = get_file_size(path_read);
}

DataManager::~DataManager() {
	if (read_file.is_open())
		read_file.close();
	if (write_file.is_open())
		write_file.close();
}

void DataManager::set_writer(std::string path_write) {
	write_file.open(path_write);
}

void DataManager::set_data(char* data, uint32_t size) {
	if ( ! write_file.is_open())
		throw std::runtime_error("DataManager is not available to write");

	write_file.write(data, size);
}

void DataManager::set_data(std::vector<char>& data) {
	if ( ! write_file.is_open())
		throw std::runtime_error("DataManager is not available to write");

	write_file.write(&*data.begin(), data.size());
}

void DataManager::set_data(std::vector<uint8_t>& data) {
	if ( ! write_file.is_open())
		throw std::runtime_error("DataManager is not available to write");

	char* begin = reinterpret_cast<char*>(data.data());
	write_file.write(begin, data.size());
}

std::vector<uint8_t>& DataManager::get_data() {
	return get_data(data_size);
}

std::vector<uint8_t>& DataManager::get_data(uint64_t size) {
	
	if (size > data_size)
		throw std::runtime_error("Invalid read overflow data");
	
	char* buffer = new char[ size ];
	read_file.read(buffer, size);
	
	out_data.clear();
	for (uint8_t i = 0; i < size; i++)
		out_data.push_back(buffer[ i ]);

	data_size -= size;
	return out_data;
}

std::string DataManager::get_line() {
	std::string line;
	getline(read_file, line);

	return line;
}

std::vector<std::string> DataManager::get_line(char separator) {
	std::string line;
	getline(read_file, line);

	return get_split(line, separator);
}

std::vector<std::string> DataManager::get_split(std::string text, char separator) {
	std::vector<std::string> result;
	uint32_t last = 0;
	for (uint32_t i = 0; i < text.length(); i++) {
		if (text[ i ] == separator) {
			result.push_back(text.substr(last, i - last));
			last = i + 1;
		}
	}

	if (last != text.length()) {
		result.push_back(text.substr(last, text.length() - 1));
	}

	return result;
}

void DataManager::set_in_buffer(char* buffer, uint64_t size) {
	if (size > data_size)
		throw std::runtime_error("Invalid read overflow data");
	read_file.read(buffer, size);
	data_size -= size;
}

void DataManager::write_single_data(std::string path_file, std::vector<std::string>& data) {
	std::ofstream current_write_file;
	current_write_file.open(path_file);

	for (std::string& line : data)
		current_write_file << line << "*";
	current_write_file.close();
}

std::string DataManager::encrypt_plain_text(std::string message, Bint& e_key, Bint& public_key) {
	uint32_t bytes_chunk = public_key.get_data().size() - 1;
	uint32_t text_size = message.size();
	uint32_t id_text = 0;


	std::vector<uint8_t> vector_complete;
	while (text_size > 0) {
		uint32_t current_chunk = text_size > bytes_chunk ? bytes_chunk : text_size;
		
		std::vector<uint8_t> vector_data(message.begin() + id_text, message.begin() + id_text + current_chunk);
		
		Bint data(vector_data);
		std::vector<uint8_t> vector_encrypt_message = Cripto::encrypt(data, e_key, public_key).get_data();
		while (vector_encrypt_message.size() < bytes_chunk + 1)
			vector_encrypt_message.push_back(0);

		vector_complete.insert(vector_complete.end(), vector_encrypt_message.begin(), vector_encrypt_message.end());

		text_size -= current_chunk;
		id_text += current_chunk;
	}

	return base64_encode(&*vector_complete.begin(), vector_complete.size());
}

std::string DataManager::decrypt_plain_text(std::string message, Bint& private_key, Bint& public_key) {
	uint32_t bytes_chunk = public_key.get_data().size() - 1;
	uint32_t id_text = 0;
	std::vector<uint8_t> vector_message = base64_decode(message);

	std::vector<uint8_t> vector_complete;
	while (id_text < vector_message.size()) {
		
		std::vector<uint8_t> vector_chunk(vector_message.begin() + id_text, vector_message.begin() + id_text + bytes_chunk + 1);
		Bint data(vector_chunk);
		std::vector<uint8_t> vector_decrypt_data = Cripto::decrypt(data, private_key, public_key).get_data();	

		vector_complete.insert(vector_complete.end(), vector_decrypt_data.begin(), vector_decrypt_data.end());

		id_text += (bytes_chunk + 1);
	}

	return std::string(vector_complete.begin(), vector_complete.end());
}

void DataManager::rename_file(std::string name) {
	std::string oldname = get_dir() + "tum_kat_temp";
	std::string newname = get_dir() + name;

	if (rename(oldname.c_str(), newname.c_str()) != 0)
		throw std::runtime_error("Fail to rename file");
}

void DataManager::remove_temporal() {
	if (remove((get_dir() + "tum_kat_temp").c_str()) != 0)
    	throw std::runtime_error("Error deleting file");
}

void DataManager::remove_readed() {
	if (remove(get_path().c_str()) != 0)
    	throw std::runtime_error("Error deleting file");
}

uint64_t DataManager::get_size() {
	return data_size;
}

std::vector<uint8_t> DataManager::get_name() {
	std::vector<uint8_t> out;

	for (int32_t i = path_in.length() - 1; i >= 0; i--) {
		if (path_in[ i ] == '/') {
			std::reverse(out.begin(), out.end());
			return out;
		}
		out.push_back(path_in[ i ]);
	}

	std::reverse(out.begin(), out.end());
	return out;
}

std::string DataManager::get_path() {
	return path_in;
}

std::string DataManager::get_dir() {
	for (int8_t i = path_in.length() - 1; i >= 0; i--) {
		if (path_in[ i ] == '/')
			return path_in.substr(0, i + 1);
	}

	return "";
}

std::string DataManager::get_format() {
	for (int8_t i = path_in.length() - 1; i >= 0; i--)
		if (path_in[ i ] == '.')
			return path_in.substr(i + 1);

	return "";
}

std::vector<uint8_t> DataManager::get_byte_hash() {
	std::string sha1 = SHA1::from_file(get_path());

	std::vector<uint8_t> out;
	for (int32_t i = sha1.length() - 1; i > 0; i -= 2)
		out.push_back(get_byte_value(sha1[ i - 1 ], sha1[ i ]));

	return out;
}

std::vector<uint8_t> DataManager::get_byte_random_mask() {
	std::vector<uint8_t> random_mask;

	for (uint32_t i = 0; i < 60; i++)
		random_mask.push_back(rand() % 256);

	return random_mask;
}

std::vector<uint8_t> DataManager::get_temporal_byte_hash() {
	write_file.close();

	std::string sha1 = SHA1::from_file(get_dir() + "tum_kat_temp");

	std::vector<uint8_t> out;
	for (int32_t i = sha1.length() - 1; i > 0; i -= 2)
		out.push_back(get_byte_value(sha1[ i - 1 ], sha1[ i ]));

	return out;
}

uint64_t DataManager::get_file_size(std::string path_file) {

	if (stat(path_file.c_str(), &results) != 0)
		throw std::runtime_error("File " + path_file + " not found");

	return results.st_size;
} 

uint8_t DataManager::get_byte_value(char& A, char& B) {
	static uint8_t f;
	static uint8_t s;
	if (A >= '0' && A <= '9')
		f = A - '0';
	else
		f = A - 'a' + 10;
	if (B >= '0' && B <= '9')
		s = B - '0';
	else
		s = B - 'a' + 10;

	return (f * 16) + s;
}











