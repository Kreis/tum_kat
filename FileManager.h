#ifndef _FILE_MANAGER_H_
#define _FILE_MANAGER_H_

#include <iostream>
#include <fstream>
#include <sys/stat.h>
#include <vector>
#include <stdlib.h>
#include <algorithm>
#include <dirent.h>
#include <stdio.h>
#include <memory>
#include <functional>
#include "Bint.h"
#include "Cripto.h"
#include "base64.h"
#include "DataManager.h"

class FileManager {
public:
	FileManager();
	static void save_keys(std::vector<Bint> vec_keys, std::string name_keys, std::string password);

	uint64_t count_file_r(std::string path_file_in, std::function<bool(std::string)> validation);
	void encrypt_file_r(std::string path_file_in,
		Bint& e_key,
		Bint& public_key,
		uint64_t total_files,
		uint64_t& current_files,
		std::function<bool(std::string)> validation,
		bool hard);
	void encrypt_file(std::shared_ptr<DataManager>& pdata_manager, Bint& e_key, Bint& public_key, bool hard);

	void decrypt_file_r(
		std::string path_file_in,
		Bint& private_key,
		Bint& public_key,
		uint64_t total_files,
		uint64_t& current_files,
		std::function<bool(std::string)> validation);
	void decrypt_file(std::shared_ptr<DataManager>& pdata_manager, Bint& private_key, Bint& public_key);

	std::pair<Bint, Bint> get_public_keys_by_file(std::string path_file);
	std::pair<Bint, Bint> get_private_keys_by_file(std::string path_file, std::string password);

private:
	void encrypt_file_metadata(
		std::vector<uint8_t>& metadata,
		std::shared_ptr<DataManager>&
		pdata_manager,
		Bint& e_key,
		Bint& public_key);
	std::vector<std::vector<uint8_t> > decrypt_file_metadata(
		std::shared_ptr<DataManager>& pdata_manager,
		Bint& private_key,
		Bint& public_key);

	void encrypt_file_data_hard(
		std::shared_ptr<DataManager>& pdata_manager,
		Bint& e_key,
		Bint& public_key);
	void decrypt_file_data_hard(
		std::vector<std::vector<uint8_t> >& metadata,
		std::shared_ptr<DataManager>& pdata_manager,
		Bint& private_key,
		Bint& public_key);

	void encrypt_file_data_soft(
		std::shared_ptr<DataManager>& pdata_manager,
		std::vector<uint8_t>& random_mask);
	void decrypt_file_data_soft(
		std::vector<std::vector<uint8_t> >& metadata,
		std::shared_ptr<DataManager>& pdata_manager,
		std::vector<uint8_t>& random_mask);

	std::string get_unique_name(std::string path_dir, bool hard);

	std::shared_ptr<Cripto> pcripto;
	uint64_t name_unique_name;
	uint32_t bytes_chunk;
};

#endif