#ifndef _BINT_H_
#define _BINT_H_

#include <iostream>
#include <vector>
#include <list>
#include <stdlib.h> 

class Bint {
public:
	Bint() {}
	Bint(std::string num) {
		Bint p( 1 );
		Bint TEN( 10 );
		Bint result( 0 );
		for (int32_t i = num.length() - 1; i >= 0; i--) {
			Bint digit(static_cast<uint8_t>(num[ i ] - '0'));
			digit *= p;
			p *= TEN;
			result += digit;
		}

		data = result.data;
	}
	Bint(std::vector<uint8_t>& num) {
		data = num;
	}
	Bint(uint64_t num) {
		while (num > 0) {
			data.push_back(num % 256);
			num /= 256;
		}
	}
	inline Bint operator * (Bint& other) {
		Bint result;
		uint64_t carry;
		uint64_t v;
		for (uint32_t i = 0; i < data.size(); i++) {
			if (get( i ) == 0)
				continue;

			carry = 0;
			for (uint32_t j = 0; j < other.data.size(); j++) {
				v = result.get(i + j) + (get( i ) * other.get( j )) + carry;

				result.put(i + j, v);
				carry = v / 256;
			}

			uint64_t other_len = other.data.size();
			while (carry > 0) {
				result.put(i + other_len, carry);
				carry /= 256;
				other_len++;
			}
		}

		return result;
	}
	inline void operator *=(Bint& other) {
		Bint A = (*this);
		Bint* B = A == other ? &A : &other;

		uint64_t carry;
		uint64_t v;

		data.clear();
		for (uint32_t i = 0; i < A.data.size(); i++) {
			if (A.get( i ) == 0)
				continue;

			carry = 0;
			for (uint32_t j = 0; j < B->data.size(); j++) {
				v = get(i + j) + (A.get( i ) * B->get( j )) + carry;

				put(i + j, v);
				carry = v / 256;
			}

			uint64_t other_len = B->data.size();
			while (carry > 0) {
				put(i + other_len, carry);
				carry /= 256;
				other_len++;
			}
		}
	}
	inline Bint operator + (Bint& other) {
		static uint32_t max_size;
		max_size = data.size() > other.data.size() ? data.size() : other.data.size();

		Bint result;
		uint64_t carry = 0;
		for (uint32_t i = 0; i < max_size; i++) {
			uint64_t v = get( i ) + other.get( i ) + carry;
			result.put(i, v);
			carry = v / 256;
		}
		while (carry > 0) {
			result.put(max_size, carry);
			carry /= 256;
			max_size++;
		}

		return result;
	}
	inline void operator +=(Bint& other) {
		static uint32_t max_size;
		max_size = data.size() > other.data.size() ? data.size() : other.data.size();

		uint64_t carry = 0;
		for (uint32_t i = 0; i < max_size; i++) {
			uint64_t v = get( i ) + other.get( i ) + carry;
			put(i, v);
			carry = v / 256;
		}
		while (carry > 0) {
			put(max_size, carry);
			carry /= 256;
			max_size++;
		}
	}
	inline Bint operator - (Bint& other) {
		if (other > (*this))
			throw std::runtime_error("not valid negative numbers");

		Bint result;
		for (uint32_t i = 0; i < data.size(); i++)
			result.data.push_back(other.get( i ) ^ 255);

		Bint ONE( 1 );
		result = result + ONE;
		result = result + (*this);
		result.resize(1);

		return result;
	}
	inline void operator -=(Bint& other) {
		if (other > (*this))
			throw std::runtime_error("not valid negative numbers");

		Bint B;
		for (uint32_t i = 0; i < data.size(); i++)
			B.data.push_back(other.get( i ) ^ 255);

		B.pp();

		(*this) += B;
		resize(1);
	}
	inline Bint operator / (Bint& other) {
		if ((*this) < other)
			return Bint(0);
		if ((*this) == other)
			return Bint(1);

		Bint A = (*this);
		Bint B = other;
		Bint C( 1 );
		Bint result;
		while (B < A) {
			B.bit_left();
			C.bit_left();
		}

		do {
			if (A >= B) {
				A -= B;
				result += C;
			}
			
			B.bit_right();
			C.bit_right();

		} while (C.data.size() > 0);

		return result;
	}
	inline void operator /=(Bint& other) {
		if ((*this) < other) {
			data.clear();
			return;
		}
		if ((*this) == other) {
			data.clear();
			data.push_back(1);
			return;
		}
		Bint A = (*this);
		Bint B = other;
		Bint C( 1 );
		
		data.clear();
		while (B < A) {
			B.bit_left();
			C.bit_left();
		}

		do {
			if (A >= B) {
				A -= B;
				(*this) += C;
			}
			
			B.bit_right();
			C.bit_right();

		} while (C.data.size() > 0);
	}
	inline Bint operator % (Bint& other) {
		if ((*this) < other) {
			return (*this);
		}
		if ((*this) == other) {
			return Bint(0);
		}
		Bint A = (*this);
		Bint B = other;
		Bint C( 1 );

		while (B < A) {
			B.bit_left();
			C.bit_left();
		}

		do {
			if (A >= B)
				A -= B;
			
			B.bit_right();
			C.bit_right();

		} while (C.data.size() > 0);

		return A;
	}
	inline void operator %=(Bint& other) {
		if ((*this) < other)
			return;
		if ((*this) == other) {
			data.clear();
			return;
		}
		Bint B = other;
		Bint C( 1 );

		while (B < (*this)) {
			B.bit_left();
			C.bit_left();
		}

		do {
			if ((*this) >= B)
				(*this) -= B;
			
			B.bit_right();
			C.bit_right();

		} while (C.data.size() > 0);
	}
	inline void pp() {
		for (uint32_t i = 0; i < data.size(); i++) {
			if (data[ i ] != 255) {
				data[ i ]++;
				for (int32_t j = i - 1; j >= 0; j--)
					data[ j ] = 0;
				return;
			}
		}

		for (uint32_t i = 0; i < data.size(); i++)
			data[ i ] = 0;
		data.push_back( 1 );
	}
	inline void mm() {
		for (uint32_t i = 0; i < data.size(); i++) {
			if (data[ i ] != 0) {
				data[ i ]--;
				for (int32_t j = i - 1; j >= 0; j--)
					data[ j ] = 255;
				resize();
				return;
			}
		}

		throw std::runtime_error("Invalid negative numbers : function mm");
	}
	inline bool is_pair() {
		return (get( 0 ) & 1) == 0;
	}
	inline void div_operation(Bint& other, std::pair<Bint, Bint>& output) {
		Bint A = (*this);
		Bint B = other;
		Bint C( 1 );
		Bint result;
		while (B < A) {
			B.bit_left();
			C.bit_left();
		}

		do {
			if (A >= B) {
				A -= B;
				result += C;
			}
			
			B.bit_right();
			C.bit_right();

		} while (C.data.size() > 0);

		output = std::pair<Bint,Bint>(result, A);
	}
	inline void bit_left() {
		for (int32_t i = data.size() - 1; i >= 0; i--) {
			if (data[ i ] & 128)
				put(i + 1, get(i + 1) | 1);
			data[ i ] <<= 1;
		}
	}
	inline void bit_right() {
		uint8_t bit = 0;
		uint8_t bit_old = 0;
		for (int32_t i = data.size() - 1; i >= 0; i--) {
			bit = data[ i ] & 1;

			data[ i ] >>= 1;
			data[ i ] |= bit_old;

			bit_old = bit ? 128 : 0;
		}

		resize();
	}
	inline bool operator < (Bint& other) {
		if (data.size() != other.data.size())
			return data.size() < other.data.size();

		for (int32_t i = data.size() - 1; i >= 0; i--)
			if (data[ i ] != other.data[ i ])
				return data[ i ] < other.data[ i ];

		return false;
	}
	inline bool operator > (Bint& other) {
		return ! (((*this) == other) || (*this) < other);
	}
	inline bool operator == (Bint& other) {
		return data == other.data;
	}
	inline bool operator != (Bint& other) {
		return data != other.data;
	}
	inline bool operator <= (Bint& other) {
		return (*this) < other || (*this) == other;
	}
	inline bool operator >= (Bint& other) {
		return (*this) > other || (*this) == other;
	}
	inline Bint spow(Bint& pow) {
		Bint module;
		return spow(pow, module);
	}
	inline Bint spow(Bint pow, Bint& module) {
		Bint result( 1 );
		Bint base = (*this);
		while (pow.data.size() > 0) {
			if (pow.is_pair()) {
				base *= base;
				if (module.data.size() != 0)
					base %= module;
				pow.bit_right();
			} else {
				result *= base;
				if (module.data.size() != 0)
					result %= module;
				pow.mm();
			}
		}

		return result;
	}
	inline void print() {
		if ( ! sign)
			std::cout << "-";

		for (int32_t i = data.size() - 1; i >= 0; i--)
			for (int8_t j = 7; j >= 0; j--)
				std::cout << ((data[ i ] & (1 << j)) != 0 ? '1' : '0');
		std::cout << std::endl;
	}
	inline std::vector<uint8_t>& get_data() {
		return data;
	}

	inline uint8_t get(uint32_t at) {
		if (data.size() <= at)
			return 0;

		return data[ at ];
	}
	inline void put(uint32_t at, uint64_t num) {
		if (num == 0)
			return;

		while (data.size() <= at)
			data.push_back( 0 );

		data[ at ] = num;
	}

private:
	bool sign = true;
	std::vector<uint8_t> data;
	std::vector<uint8_t> data_left;

	inline void resize(uint32_t ignore = 0) {
		for (int32_t i = data.size() - 1 - ignore; i >= 0; i--)
			if (data[ i ] != 0) {
				data.resize(i + 1);
				return;
			}
		data.clear();
	}

};

#endif







