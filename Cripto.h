#ifndef _CRIPTO_H_
#define _CRIPTO_H_

#include <iostream>
#include <vector>
#include <ctime>
#include "Bint.h"
#include "sha1.h"
#include "DataManager.h"

class Cripto {
public:
	Cripto();
	std::vector<Bint> generate_keys(uint64_t n_bits_of_key);
	Bint generate_e(Bint& pq_m1);
	bool validate_e(Bint& pq_m1, uint64_t e);
	std::pair<Bint, Bint> generate_p_q(uint64_t n_bits_of_key);
	Bint generate_prime_number(uint64_t n_bytes);
	Bint generate_private_key(Bint& pq_1, Bint& e);

	static Bint encrypt(Bint& data, Bint& e_key, Bint& public_key);
	static Bint decrypt(Bint& data, Bint& private_key, Bint& public_key);
	static std::vector<uint8_t> get_complex_password(std::string password);
private:
	void generate_sieve_primes(uint32_t limit);
	bool final_test(Bint& e_key, Bint& private_key, Bint& public_key);
	bool miller_rabin_individual_test(Bint n, Bint d, uint64_t int_k);
	bool miller_rabin(Bint n, uint64_t i);
	bool fermat_little(Bint n, uint64_t i);

	bool is_prime(uint64_t num);
	uint64_t get_random(uint32_t num_bits);
	Bint gcd(Bint& A, Bint& B);

	Bint ZERO;
	Bint ONE;
	Bint TWO;
	std::vector<Bint> primes;
};

#endif